FROM python:3.8
ENV PYTHONUNBUFFERED 1
WORKDIR /usr/src/bootcamp
COPY requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY . .
