from django.db import models

# Create your models here.
class Marca(models.Model):
    title = models.CharField(max_length = 100)
    def __str__(self):
        return self.title 

class Product (models.Model):
    title = models.CharField(max_length = 100)
    description = models.CharField(max_length = 300)
    value = models.FloatField()
    image = models.ImageField(upload_to = 'products', null = True, blank = True)
    marca = models.ForeignKey(Marca, on_delete = models.PROTECT)

    def __str__(self):
        return self.title 
